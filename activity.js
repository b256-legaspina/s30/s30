//Objective 1
//count operator to count the total number of fruits on sale

db.fruits.aggregate([
{$match: {onSale: true}},
{$count: "numberOfOnSaleFruits"},
{$out: "numberOfSaleFruits"}
]);

//Objective 2

// count the total number of fruits with stock more than or equal to 20

db.fruits.aggregate([
{$match: {$and: [
    {stock: {$gte: 20}}
    ]}},
{$count: "numberOfFruitsWithStockMoreThanOrEqualTo20"},
{$out: "enoughStock"}

]);

//Objective 3

// average price of fruits onSale per supplier

db.fruits.aggregate([
{$match: {onSale: true}},
{$group: {_id: "$supplier_id", avgPrice: {$avg: "$price"}}},
{$out: "avg_price"}
]);

// Objective 4

// max operator to get the highest price of a fruit per supplier

db.fruits.aggregate([
{$match: {onSale: true}},
{$group: {_id: "$supplier_id", maxPrice: {$max: "$price"}}}
{$out: "max_price"}
]);

// Objective 5
// lowest price of a fruit per supplier.

db.fruits.aggregate([
{$match: {onSale: true}},
{$group: {_id: "$supplier_id", maxPrice: {$min: "$price"}}},
{$out: "min_price"}
]);